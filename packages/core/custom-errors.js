class AppError extends Error {
    constructor(message, data) {
        super(message)
        this.name = this.constructor.name
        this.data = data
    }

}

class NotFoundError extends AppError { }

module.exports = {
    AppError,
    NotFoundError
}