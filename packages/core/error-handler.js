const logger = require('./logger')
const dbErrors = require('db-errors')

module.exports = function (err, req, res, next) {
  if (err.isJoi) {
    return res.status(400).send({
      statusCode: 400,
      message: 'Bad Input',
      details: err.message
    })
  }

  if (err.isAxiosError) {
    if (err.code === 'ECONNABORTED') {
      return res.status(408).send({
        statusCode: 408,
        message: "Timeout",
        details: err.message
      })
    }

    if (err.response) {
      return res.status(err.response.status).send(err.response.data)
    }

  }

  if (err.isBoom) {
    res.status(err.output.statusCode).send({
      ...err.output.payload
    })
  }

  if (err.sqlState) {
    const dbErr = dbErrors.wrapError(err)

    console.log(dbErr);

    return res.status(400).send({
      message: dbErr.name,
      details: dbErr.constraint
    })
  }

  if (err.name === 'JsonWebTokenError') {
    return res.status(401).send({
      message: err.name,
      details: err.message
    })
  }

  logger.error(err)

  res.status(500).send({
    statusCode: 500,
    message: 'Internal Error'
  })
}
