const bodyParser = require('body-parser');
const errorHandler = require('../core/error-handler')
const logger = require('./logger')

function createServer(callback = () => { }) {
  const app = require('express')();

  app.use(bodyParser.json());

  callback(app)

  app.use(errorHandler)

  process.on('uncaughtException', err => {
    logger.error(err)

    process.exit(1)
  })

  process.on('unhandledRejection', (err) => {
    console.log(err);
    throw new Error('unhandledRejection')
  })

  return app
}



module.exports = createServer;
