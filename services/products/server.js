const createServer = require('../../packages/core/create-server')

const productRouter = require('./api/index')

const app = createServer((app) => {
  app.get('/', (req, res, next) => {

    res.send('Hello')
  })

  app.use('/api', productRouter)
})

module.exports = app;
