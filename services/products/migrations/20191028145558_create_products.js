exports.up = async function (knex) {

    await knex.schema.createTable('productCategories', t => {
        t.increments('id');
        t.string('name').notNullable();
    });

    return knex.schema.createTable('products', t => {
        t.increments('id');
        t.string('name').notNullable();
        t.decimal('price').notNullable();
        t.integer('categoryId').unsigned();
        t.foreign('categoryId').references('productCategories.id');
    });
};

exports.down = async function (knex) {
    await knex.schema.dropTable('products');
    return knex.schema.dropTable('productCategories');
};