
exports.up = async function (knex) {
    const { count } = await knex('productCategories')
        .count({ count: '*' })
        .first();
    if (count > 0) {
        return;
    }

    return knex('productCategories').insert([
        { name: 'Shoes' },
        { name: 'Clothing' },
        { name: 'Accessories' }
    ]);
};

exports.down = function () { };

