
exports.up = function (knex) {
  return knex.schema.createTable('productImages', t => {
    t.increments('id');
    t.string('imageUrl').notNullable();
    t.integer('productId')
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('productImages');
};
