const knexConfig = require('../knexfile')
const knex = require('knex')(knexConfig)

const Boom = require('@hapi/boom')

const PRODUCTS = 'products'

async function create(data) {
  const result = await knex(PRODUCTS).insert(data)

  return result[0]
}

async function getByIds(ids = []) {
  const result = await knex(PRODUCTS)
    .select('*')
    .whereIn('id', ids)

  return result
}

async function getById(id) {
  const result = await knex(PRODUCTS)
    .first('*')
    .where({ id })

  if (!result) {
    throw Boom.notFound('product not found')
  }

  return result
}

module.exports = {
  create,
  getByIds,
  getById
}
