const knexConfig = require('../knexfile')
const knex = require('knex')(knexConfig)

const PRODUCT_IMAGES = 'productImages'

async function create(data) {

  const result = await knex(PRODUCT_IMAGES).insert(data)

  return result[0]
}

async function getByIds(ids = []) {
  const result = await knex(PRODUCT_IMAGES)
    .select('*')
    .whereIn('id', ids)

  return result
}

async function getByProductsId(productIds) {
  const result = await knex(PRODUCT_IMAGES)
    .select('*')
    .whereIn('productId', productIds)

  return result
}

module.exports = {
  create,
  getByIds,
  getByProductsId
}


