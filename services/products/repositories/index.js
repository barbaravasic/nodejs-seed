module.exports = {
  products: require('./products'),
  productCategories: require('./product-categories'),
  productImages: require('./product-images')
}
