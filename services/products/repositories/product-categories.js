const knexConfig = require('../knexfile')
const knex = require('knex')(knexConfig)

const PRODUCT_CATEGORIES = 'productCategories'

async function create(data) {
  const result = await knex(PRODUCT_CATEGORIES).insert(data)

  return result
}

async function getByIds(ids = []) {
  const result = await knex(PRODUCT_CATEGORIES)
    .select('*')
    .whereIn('id', ids)

  return result
}

module.exports = {
  create,
  getByIds
}
