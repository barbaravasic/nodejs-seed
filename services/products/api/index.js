const router = require('express').Router()
const asyncHandler = require('express-async-handler')
const asyncBusBoy = require('async-busboy')

const createProduct = require('./create-product')
const productImages = require('./create-product-image')
const getById = require('./get-by-id')

router.post('/products', asyncHandler(async (req, res) => {

  const data = req.body

  const result = await createProduct(data)

  res.send(result)
}))


router.get('/products/:id', asyncHandler(async (req, res) => {


  const result = await getById(req.params.id)

  res.send(result)
}))

router.post('/products/:id/images', asyncHandler(async (req, res) => {
  const { files } = await asyncBusBoy(req)

  const productId = req.params.id

  const image = await productImages.createProductImage(productId, files[0])

  res.send(image)
}))


module.exports = router
