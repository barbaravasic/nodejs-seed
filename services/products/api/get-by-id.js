const Joi = require('@hapi/joi')
const db = require('../repositories')

const productPresenter = require('./product-presenter')

const schema = Joi.number().required()


module.exports = async (data) => {

  const id = Joi.attempt(data, schema)

  const product = await db.products.getById(id)


  const result = await productPresenter([product.id])

  return result[0]
}
