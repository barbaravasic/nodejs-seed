const AWS = require('aws-sdk')
const config = require('../config')

const sqs = new AWS.SQS({
  credentials: {
    accessKeyId: config.aws.accessKeyId,
    secretAccessKey: config.aws.secretAccessKey
  },
  region: config.aws.region

})

function publish(type, payload) {

  return sqs
    .sendMessage({
      QueueUrl: config.aws.sqsQueueUrl,
      MessageBody: JSON.stringify({
        type,
        payload
      })
    })
    .promise()
    .catch(err => console.error(err))
}

module.exports = publish
