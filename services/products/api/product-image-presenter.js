const db = require('../repositories')

async function present(imageIds) {

  const images = await db.productImages.getByIds(imageIds)

  const result = images.map(image => {

    return {
      id: image.id,
      imageUrl: image.url,
      productId: image.productId
    }
  })

  return result
}

module.exports = present
