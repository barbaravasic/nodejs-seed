const db = require('../repositories')

async function present(productIds) {

  const products = await db.products.getByIds(productIds)

  const categoryIds = products.map(product => {
    return product.categoryId
  })

  const [categories, images] = await Promise.all([
    db.productCategories.getByIds(categoryIds),
    db.productImages.getByProductsId(productIds)
  ])

  const result = products.map(product => {
    const category = categories.find(cat => cat.id === product.categoryId)

    const productImages = images.filter(img => img.productId === product.id)

    return {
      ...product,
      category,
      images: productImages
    }
  })

  return result
}

module.exports = present
