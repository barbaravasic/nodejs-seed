const AWS = require('aws-sdk')
const Joi = require('@hapi/joi')
const uuid = require('uuid/v4')
const path = require('path')

const db = require('../repositories')

const productImagePresenter = require('./product-image-presenter')


const config = require('../config')

const s3 = new AWS.S3({
  credentials: {
    accessKeyId: config.aws.accessKeyId,
    secretAccessKey: config.aws.secretAccessKey
  },
  endpoint: config.aws.s3Endpoint,
  s3BucketEndpoint: !!config.aws.s3Endpoint,
  s3ForcePathStyle: true
})

const schema = Joi.object({
  imageUrl: Joi.string().uri().required(),
  productId: Joi.number().required()
})

async function createProductImage(productId, file) {

  const objectKey = `${uuid()}${path.extname(file.filename)}`

  const uploadResult = await s3.upload({
    Bucket: config.products.imagesBucket,
    Key: objectKey,
    Body: file
  })
    .promise()

  const data = {
    imageUrl: uploadResult.Location,
    productId
  }

  const imageData = Joi.attempt(data, schema)

  const imageId = await db.productImages.create(imageData)

  const result = await productImagePresenter([imageId])

  return result[0]
}

module.exports = {
  createProductImage
}
