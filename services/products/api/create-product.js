const Joi = require('@hapi/joi')
const db = require('../repositories')

const publish = require('./publisher')

const productPresenter = require('./product-presenter')

const schema = Joi.object({
  name: Joi.string().required(),
  price: Joi.number().required(),
  categoryId: Joi.number().required()
})

module.exports = async (data) => {

  const prodData = Joi.attempt(data, schema)

  const id = await db.products.create(prodData)

  const result = await productPresenter([id])

  publish('PRODUCT_CREATED', result)

  return result[0]
}
