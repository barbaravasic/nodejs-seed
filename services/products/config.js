require('dotenv').config({ path: `${__dirname}/.env` });

const { env } = process

module.exports = {
  app: {
    name: env.APP_NAME,
    port: env.PORT || 3000
  },
  db: {
    name: env.DB_NAME,
    user: env.DB_USER,
    password: env.DB_PASSWORD,
    host: env.DB_HOST
  },
  aws: {
    accessKeyId: env.AWS_ACCESS_KEY_ID,
    secretAccessKey: env.AWS_SECRET_ACCESS_KEY,
    region: env.AWS_REGION,
    s3Endpoint: env.AWS_S3_ENDPOINT,
    sqsQueueUrl: env.AWS_SQS_QUEUE_URL
  },
  products: {
    imagesBucket: env.PRODUCT_IMAGES_BUCKET
  }
}
