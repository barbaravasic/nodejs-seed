const request = require('supertest');
const server = require('../server');
const db = require('../repositories');

jest.mock('../repositories');

const token = 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTMsImlhdCI6MTU3MjM2MzEwNn0.I1gDl80fjfsbvI_vNaaJVmfB2aF5DklSEeRxkoqOq4JWr2_PC9-iav39O8NJfRhYV2xsi6f8NnwDbn6yRg09I0ucYLm8SgJbTG2hfITCSEFkOpHNLyxaPD29dUmwb_smyhHUzqOEyROo6RRYDYj7x_y4ZSYgsbuouzi3kd1fDrhiiE09EHL5qQCUOcKP8mPjhxeyFl-EOTmI6uqD81_Z722VEqdO8nidiRoadIt_cZS1NziRfvmFYqqGBNVfpn-60hgVOmNBUL6o1CsrsAxUIIzKLNBF-r6lsUHNG7o5iQBTta0n6m7GKZ7orn2RCN9yPvztp_zjnYnygge0_v6eRQ'

describe('POST /products', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('creates product', async () => {
    const product = {
      id: 1,
      name: 'Test',
      price: 200,
      categoryId: 1
    };

    const category = {
      id: 1,
      name: 'test'
    };

    const image = {
      id: 1,
      imageUrl: 'http://localhost:4572/pw-product-images/f4150ff7-d0c8-47b7-837f-151a6ba50180.PNG',
      productId: 1
    }

    db.products.create.mockResolvedValue(product.id);

    db.products.getByIds.mockResolvedValue([product]);

    db.productCategories.getByIds.mockResolvedValue([category]);

    db.productImages.getByProductsId.mockResolvedValue([image]);

    const response = await request(server)
      .post('/api/products')
      .set('Authorization', token)
      .send({
        name: product.name,
        price: product.price,
        categoryId: product.categoryId
      });

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      ...product,
      category: {
        ...category
      },
      images: [image]
    });
  });

  it('validation', async () => {
    const product = {
      id: 1,
      name: 'Test',
      categoryId: 1
    };

    const category = {
      id: 1,
      name: 'test'
    };

    const response = await request(server)
      .post('/api/products')
      .set('Authorization', token)
      .send({
        name: product.name,
        categoryId: category.id
      });

    expect(response.status).toBe(400);
    expect(response.body.message).toMatchInlineSnapshot(`"Bad Input"`);
    expect(response.body.details).toMatchInlineSnapshot(
      `"\\"price\\" is required"`
    );
  });
});
