const router = require('express').Router()
const asyncHandler = require('express-async-handler')
const asyncBusBoy = require('async-busboy')
const memoize = require('memoizee')

const productSvc = require('../../proxies/productSvc')

const authMiddleware = require('../../middlewares/auth-middleware')

// const getById = memoize(productSvc.getById, {
//   promise: true
// })


router.post('/products', authMiddleware, asyncHandler(async (req, res) => {

  const data = req.body

  const result = await productSvc.create(data)

  res.send(result)
}))


router.get('/products/:id', asyncHandler(async (req, res) => {


  const result = await productSvc.getById(req.params.id)

  res.send(result)
}))

router.post('/products/:id/images', asyncHandler(async (req, res) => {
  const { files } = await asyncBusBoy(req)

  const productId = req.params.id

  const image = await productSvc.addProductImage(productId, files[0])

  res.send(image)
}))


module.exports = router
