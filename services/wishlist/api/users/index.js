const router = require('express').Router()
const asyncHandler = require('express-async-handler')

const createUsers = require('./create-user')
const authUser = require('./auth-user')

router.post('/users', asyncHandler(async (req, res) => {

    const data = req.body

    const result = await createUsers(data)

    res.send(result)
}))

router.post('/users/auth', asyncHandler(async (req, res) => {

    const data = req.body

    const result = await authUser(data)

    res.send(result)
}))

module.exports = router