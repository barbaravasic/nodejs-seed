const Joi = require('@hapi/joi')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const config = require('../../config')

const userPresenter = require('./user-presenter')

const db = require('../../repositories')

const schema = Joi.object({
    name: Joi.string().required(),
    username: Joi.string().required(),
    password: Joi.string().min(5).max(15).required(),
    gender: Joi.string().valid('M', 'W', 'K').required(),

})

module.exports = async (data) => {

    const userData = Joi.attempt(data, schema)

    const passwordHashed = await bcrypt.hash(userData.password, 10)

    const id = await db.users.create({
        ...userData,
        password: passwordHashed
    })

    const token = jwt.sign({ id }, {
        key: config.jwt.privateKey,
        passphrase: config.jwt.passPhrase
    }, {
        algorithm: 'RS256'
    })

    const result = await userPresenter([id])

    return {
        user: result[0],
        token
    }
}