const Joi = require('@hapi/joi')
const bcrypt = require('bcrypt')
const Boom = require('@hapi/boom')
const jwt = require('jsonwebtoken')

const config = require('../../config')

const userPresenter = require('./user-presenter')

const db = require('../../repositories')

const schema = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().min(5).max(15).required()
})

module.exports = async (data) => {

    const userData = Joi.attempt(data, schema)

    const user = await db.users.getByUsername(userData.username)

    const passwordMatch = await bcrypt.compare(userData.password, user.password)

    if (!passwordMatch) {
        throw Boom.notFound('Invalid password')

    }

    const userId = user.id

    const token = jwt.sign({ userId }, {
        key: config.jwt.privateKey,
        passphrase: config.jwt.passPhrase
    }, {
        algorithm: 'RS256'
    })

    const result = await userPresenter([user.id])

    return {
        user: result[0],
        token
    }
}