const db = require('../../repositories')

async function present(userIds) {
    const users = await db.users.getByIds(userIds)

    const result = users.map(user => {
        const { password, ...rest } = user

        return rest
    })

    return result
}

module.exports = present