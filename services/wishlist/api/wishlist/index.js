const router = require('express').Router()
const asyncHandler = require('express-async-handler')

const createWishlist = require('./create-wishlist')


router.post('/wishlists', asyncHandler(async (req, res) => {

  const data = req.body

  const result = await createWishlist(data)

  res.send(result)
}))

module.exports = router
