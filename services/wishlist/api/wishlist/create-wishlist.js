const Joi = require('@hapi/joi')
const wishlistRepo = require('../../repositories/wishlists')

const wishlistPresenter = require('./wishlist-presenter')


const schema = Joi.object({
  name: Joi.string().required()
})

module.exports = async (data) => {

  const wishlistData = Joi.attempt(data, schema)

  const id = await wishlistRepo.create(wishlistData)

  const result = await wishlistPresenter([id])

  return result[0]

}
