const wishlistRepo = require('../../repositories/wishlists')

async function present(wishlistIds) {

    const wishlists = await wishlistRepo.getByIds(wishlistIds)

    const result = wishlists.map(wishlist => {
        return {
            id: wishlist.id,
            name: wishlist.name
        }
    })

    return result
}

module.exports = present