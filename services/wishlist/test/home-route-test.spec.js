const request = require('supertest')
const server = require('../server')

describe('GET /', () => {

    it('responds with json', async () => {
        const response = await request(server)
            .get('/')

        expect(response.status).toBe(200)
        expect(response.body).not.toBe('')
    })
})