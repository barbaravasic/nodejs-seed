const knexConfig = require('../knexfile')
const knex = require('knex')(knexConfig)

const Boom = require('@hapi/boom')

const USERS = 'users'

async function create(data) {
  const result = await knex(USERS).insert(data)

  return result[0]
}

async function getByIds(ids = []) {
  const result = await knex(USERS)
    .select('*')
    .whereIn('id', ids)

  return result
}

async function getByUsername(username) {
  const result = await knex(USERS)
    .first('*')
    .where({ username })

  if (!result) {
    throw Boom.notFound('user not found')
  }

  return result
}

module.exports = {
  create,
  getByIds,
  getByUsername
}
