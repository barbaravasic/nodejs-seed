const knexConfig = require('../knexfile')
const knex = require('knex')(knexConfig)

const WISHLISTS = 'wishlists'

async function create(data) {
  const result = await knex(WISHLISTS).insert(data)

  return result[0]
}

async function getByIds(ids = []) {
  const result = await knex(WISHLISTS)
    .select('*')
    .whereIn('id', ids)

  return result
}

module.exports = {
  create,
  getByIds
}
