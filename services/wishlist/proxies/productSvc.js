const axios = require('axios')
const config = require('../config')

const FormData = require('form-data');

const instance = axios.create({
  baseURL: `${config.products.host}`,
  timeout: 10000
})

module.exports = {
  getById: async id => (await instance.get(`/products/${id}`).data),
  create: async data => (await instance.post('/products', data)).data,
  addProductImage: async (id, file) => {
    const formData = new FormData();
    formData.append('file', file);
    return (await instance.post(`/products/${id}/images`, formData, {
      headers: {
        'Content-Type': `multipart/form-data; boundary=${formData._boundary}`
      }
    })).data;
  }
}

