const createServer = require('../../packages/core/create-server')

const productRouter = require('./api/products/index')
const wishlistRouter = require('./api/wishlist/index')
const userRouter = require('./api/users/index')

const app = createServer((app) => {
  app.get('/', (req, res, next) => {

    res.send('Hello')
  })

  app.use('/api', productRouter)
  app.use('/api', wishlistRouter)
  app.use('/api', userRouter)
})

module.exports = app;
