
exports.up = function (knex) {
    return knex.schema.createTable('users', t => {
        t.increments('id');
        t.string('name');
        t.string('username');
        t.string('password');
        t.string('gender');
        t.timestamp('createdAt', { precision: 6 }).defaultTo(knex.fn.now(6))

        t.unique('username')
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('users');
};
