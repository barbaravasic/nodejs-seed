const config = require('./config')
const server = require('./server');

const logger = require('./logger')

server.listen(config.app.port, () => {
  logger.info(`${config.app.name} started on port ${config.app.port}`);
})

